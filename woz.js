#!/usr/bin/env node

var http = require("http"),
    url = require("url"),
    path = require("path"),
    dir = require('node-dir'),
    swig  = require('swig'),
    fs = require("fs"),
    express = require("express"),

    SERVER = "server",
    BUILD = "build",
    NEW = "new",

    SOURCE_FOLDER = "source",
    BUILD_FOLDER = "build",


    command = process.argv[2] || SERVER,
    project = process.argv[3] || "noname",
    port = process.argv[4] || 8888

    ;

    console.log( "Params:", process.argv );

var deleteFolderRecursive = function(road) {
    var files = [];
    if( fs.existsSync(road) ) {
        files = fs.readdirSync(road);
        files.forEach(function(file,index){
            var curPath = path.join(road,file); 
            console.log("curPath:", curPath);
            //console.log("Resolve Path:", path.join(road,file));
            if(fs.lstatSync(curPath).isDirectory()) { // recurse
                deleteFolderRecursive(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(road);
    }
};

//Create Server
if ( command  == SERVER ){ 

  var app = express();
  // This is where all the magic happens!
  app.engine('html', swig.renderFile);

  app.set('view engine', 'html');
  app.set('views', path.join( process.cwd(), SOURCE_FOLDER ));

  var request,template;

  app.all('/', function(req, res,next){
    //res.sendfile("index.html");
    request = "/index";
    template = request.replace("/","");
    
    console.log("Request", request, " Params:", template);
    next();
  });

  app.all('*.html', function(req, res,next){
    //res.sendfile("index.html");
    request = req.params[0];
    template = request.replace("/","");
    
    console.log("Request", request, " Params:", template);
    next();
  });
 

  app.get(/((.+)(\.html))/, function (req, res) {

    console.log("Request GET", request, " Params:", template);
    res.render( template +'.html'); // will render home.jade and send the HTML
  });

  app.get("/", function (req, res) {
    console.log("Request GET", request, " Params:", template);
    res.render( template +'.html'); // will render home.jade and send the HTML
  });


  app.use( express.static( path.join( process.cwd(), SOURCE_FOLDER ) ) );
  app.listen(port);

  console.log("Static file server running at\n  => http://localhost:" + port + "/\nCTRL + C to shutdown");

}
// Create Build
else if (command == BUILD ){

  console.log("Create Build");

  var folder = path.join(process.cwd(), BUILD_FOLDER);
  //Create Build Folder
  deleteFolderRecursive(folder);
  fs.mkdir(folder);

  // match only filenames with a .html extension and that don't start with a `.´
  dir.readFiles(process.cwd(), {
      match: /.$/,
      exclude: /^\./
      }, function(err, content, next) {
          if (err) throw err;
 
          next();
      },
      function(err, files){
          if (err) throw err;
          for (var i = 0; i<files.length; i++) {
            var file = files[i],
                filename = path.basename(file),
                dir = file.replace(filename,'').replace(SOURCE_FOLDER,BUILD_FOLDER) ;
            
            if (!fs.existsSync(dir)) {  
                fs.mkdir(dir);  
            }  

            console.log("File:", file.split("/"), " Dir:", dir);
            if( /\.html/.test(file) ){
              fs.writeFile(file.replace(SOURCE_FOLDER,BUILD_FOLDER), swig.renderFile(file), function(err) {
                if(err) {
                    console.log(err);
                } else {
                    console.log("The file just saved!", file.replace(SOURCE_FOLDER,BUILD_FOLDER) );
                }
              });
            }else{
              fs.writeFile(file.replace(SOURCE_FOLDER,BUILD_FOLDER),fs.readFileSync(file) , function(err) {
                if(err) {
                    console.log(err);
                } else {
                    console.log("The file just saved!", file.replace(SOURCE_FOLDER,BUILD_FOLDER) );
                }
              });
            }
            /*fs.writeFile(file.replace(SOURCE_FOLDER,BUILD_FOLDER), swig.renderFile(file), function(err) {
                if(err) {
                    console.log(err);
                } else {
                    console.log("The file just saved!", file.replace(SOURCE_FOLDER,BUILD_FOLDER) );
                }
            });
            */ 
          }
          //console.log('finished reading files:',files);
      });

}
// Create Build
else if (command == NEW ){
  var folder = path.join(process.cwd(), project),
      source = path.join(folder,SOURCE_FOLDER)
      ;
  fs.mkdirSync(folder);
  fs.mkdirSync(source);
  console.log("Create project:", project );
}

